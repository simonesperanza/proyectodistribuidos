import os

folder = "LogsTest/"
filepathList = []

def last_5chars(x):
    return(x[-5:])
### OBTENGO LA LISTA DE LOS PATH PARA HACER LA CONCATENACION
for file in os.listdir(folder):
    filepath = os.path.join(folder,file)
    filepathList.append(filepath)
#### ORDENO DE MENOR A MAYOR
#print(filepathList)
filepathList = sorted(filepathList, key = last_5chars)
#print(filepathList)
#### PROCEDO A CREAR EL .LOG FINAL
with open('grouped_logs_all','w') as outfile:
    for fname in filepathList:
        with open(fname) as infile:
            outfile.write(infile.read())