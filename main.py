import os
import re
import logging
import geoip2.database
import sys
from mpi4py import MPI
from datetime import date
from datetime import datetime

logging.basicConfig(filename='project.log', filemode='w', format='%(levelname)s:%(asctime)s:%(message)s', level=logging.DEBUG) # Logger configuracion
file_path = "/local_home/slesperanza.15/proyectodistribuidos/" #os.path.dirname(os.path.abspath(__file__)) # Ruta del archivo main.py
log_file = open("/local_home/slesperanza.15/proyectodistribuidos/Logs/grouped_logs.log") # Archivo de Logs
ip_re = re.compile(r"oip=\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}") # Expresion Regular para direcciones IP
user_re = re.compile(r"account=\S+@\S+") # Expresion Regular Simple para Correos
hour_re = re.compile(r"\d{1,2}:") # Expresion Regular Simple para el Tiempo
country_reader = geoip2.database.Reader(file_path + '/GeoLite2-Country.mmdb') # Pais de una IP
city_reader = geoip2.database.Reader(file_path + '/GeoLite2-City.mmdb') # Ciudad de una IP
argumento = int(sys.argv[1])


line = ""                         # Linea del archivo Log
total_lines = 0                   # Numero total de lineas en el archivo de Log
countries_dictionary = {}         # Diccionario de paises fuentes de ataques
cities_dictionary = {}            # Diccionario de ciudades fuentes de ataques
ip_dictionary = {}                # Diccionario de IPs fuentes de ataques
user_dictionary = {}              # Diccionario de Autores fuentes de ataques
hour_dictionary = {               # Diccionario de Autores fuentes de ataques
    '00:':0,'01:':0,'02:':0,'03:':0,
    '04:':0,'05:':0,'06:':0,'07:':0,
    '08:':0,'09:':0,'10:':0,'11:':0,
    '12:':0,'13:':0,'14:':0,'15:':0,
    '16:':0,'17:':0,'18:':0,'19:':0,
    '21:':0,'22:':0,'23:':0
}
days_dictionary = {}              # Diccionario de dias
protocols_dictionary = {"Imap": 0, "Pop": 0, "soap": 0} # Diccionario de protocolos (Conexiones fallidas)
success_protocols_dictionary = {"Imap": 0, "Pop": 0, "soap": 0} # Diccionario de conexiones exitosas por protocolo
time_mark = datetime(2018, 1, 1) # Marca de tiempo para medir bloques de X minutos
attacks_average = {} # Diccionario de listas de ataques por cada X minutos por pais
temporal_attacks_average = {} # Diccionario de listas de ataques por cada X minutos por pais
                              # (se borrar al detectar nuevo bloque de 5 minutos)

# Return true if there is a difference greater than 'argumento'
# between 'date_string' and 'time_mark'
def check_time_delta(date_string):
    global time_mark
    global argumento

    # Obtains datetime object from string
    # Format(year, month, day, hour, minute, second)
    date = get_date_from_string(date_string)

    delta = date - time_mark
    minutes = delta.seconds // 60
    if minutes > argumento:
        return True
    else:
        return False

def get_date_from_string(date_string):
    
    # Obtains datetime object from string
    # Format(year, month, day, hour, minute, second)
    try:
        return datetime(
        int(date_string[0:4]), int(date_string[5:7]), int(date_string[8:10]),
        int(date_string[11:13]), int(date_string[14:16]), int(date_string[17:19]))
    except Exception as e:
        print("get_date_from_string: Excepcion: {1}".format(str(e)))
    
    


def attacks_by_user():
    global user_dictionary
    global log_file
    global line

    user_list = user_re.findall(line)   # Devuelvo la linea
    if len(user_list) > 0:  # Si la linea contiene la expresion regular de correo, tiene autor
        user = user_list[0].replace("account=","",1)    # Separo de la linea la informacion que no me interesa para solo dejar el payload
        if user in user_dictionary: # Reviso si el usuario se encuentra dentro del diccionario 
            user_dictionary[user] = user_dictionary[user] + 1 # Si el usuario se encuentra en el diccionario, cuento como un nuevo ataque
        else:   # Si no se encuentra dentro del diccionario lo registro
            user_dictionary[user] = 1

def attacks_by_hour():
    global hour_dictionary
    global log_file
    global line

    hour_list = hour_re.findall(line)   # Devuelvo la linea
    if len(hour_list) > 0:  # Si la linea contiene la expresion regular de hora
        hour = hour_list[0]    # Separo de la linea la informacion que no me interesa para solo dejar el payload
        if hour in hour_dictionary: # Reviso si la hora se encuentra dentro del diccionario 
            hour_dictionary[hour] = hour_dictionary[hour] + 1 # Si la hora se encuentra en el diccionario, cuento como un nuevo ataque
        else:   # Si no se encuentra dentro del diccionario lo registro
            hour_dictionary[hour] = 1

def attacks_by_ip():
    global countries_dictionary
    global cities_dictionary
    global ip_dictionary
    global time_mark
    global log_file
    global line
    try:
        ip_list = ip_re.findall(line)  # Buscar IP de origen de ataque
        if len(ip_list) > 0:          # Si contiene ip, limpiar el string 
            ip = ip_list[0].replace("oip=","",1) # Leer linea y obtener IP
            country = country_reader.country(ip).country.name # Pais de la IP
            city = city_reader.city(ip).city.name # Ciudad de la IP
            ####################### IP #######################
            if ip in ip_dictionary: # Nuevo ataque
                ip_dictionary[ip] = ip_dictionary[ip] + 1
            else: # Registrar nueva IP atacante
                ip_dictionary[ip] = 1
            ###################### PAIS ######################
            if country in countries_dictionary: # Nuevo ataque
                countries_dictionary[country] = countries_dictionary[country] + 1
            else: # Registrar nuevo pais atacante
                countries_dictionary[country] = 1
            ##################### CIUDAD #####################
            if city in cities_dictionary:       # Nuevo ataque
                cities_dictionary[city] = cities_dictionary[city] + 1
            else: # Registrar nueva ciudad atacante
                cities_dictionary[city] = 1
            ############### PROMEDIO POR PAIS ################
            # if check_time_delta(line): # Si hay nuevo bloque de X minutos
            #     time_mark = get_date_from_string(line)
            #     append_dictionary_values(attacks_average, temporal_attacks_average)
            #     temporal_attacks_average.clear()
            #     temporal_attacks_average[country] = 1
            # else:
            #     if country in temporal_attacks_average:
            #         temporal_attacks_average[country] += 1
            #     else:
            #         temporal_attacks_average[country] = 1
    except geoip2.errors.AddressNotFoundError as e: # No se encontro informacion de la IP
        pass
    except Exception as e:
        print("attacks_by_ip Excepcion: {1}".format(str(e)))
        #logging.error("Mensaje excepcion: {0} \nPosicion Buffer: {1} \nLinea: {2}".format(str(e), log_file.tell(), line))

def attacks_by_protocols():
    global line
    global protocols_dictionary
    
    imap = "Imap"
    pop = "Pop"
    soap = "soap"

    if imap in line:
        protocols_dictionary[imap] += 1
    elif pop in line:
        protocols_dictionary[pop] += 1
    else:
        protocols_dictionary[soap] += 1

def attacks_by_day():
    global line
    global days_dictionary
    fecha = line[:10]
    if fecha in days_dictionary:
        days_dictionary[fecha] += 1
    else: # Registrar nueva IP atacante
        days_dictionary[fecha] = 1

def success_protocols_connections():
    global line
    global success_protocols_dictionary
    
    imap = "Imap"
    pop = "Pop"
    soap = "soap"

    if imap in line:
        success_protocols_dictionary[imap] += 1
    elif pop in line:
        success_protocols_dictionary[pop] += 1
    else:
        success_protocols_dictionary[soap] += 1

# Suma los valores del diccionario d2 a los de d
def add_dictionary_values(d, d2):
    try:
        for key in d2:
            if d.__contains__(key):
                d[key] = d[key] + d2[key]
        else:
            d[key] = d2[key] 
    except Exception as e:
        print("add_dictionary_values Excepcion: {1}".format(str(e)))
    

# Concatena listas en un diccionario
def append_dictionary_lists(d, d2):
    try:
        for key in d2:
            if d.__contains__(key):
                d[key].extend(d2[key])
        else:
            d[key] = d2[key]
    except Exception as e:
        print("append_dictionary_lists Excepcion: {1}".format(str(e)))
    

# Concatena valores de lista en un diccionario
def append_dictionary_values(d, d2):
    try:
        for key in d2:
            if d.__contains__(key):
            	d[key].append(d2[key])
            else:
            	d[key] = [d2[key]]
    except Exception as e:
        print("append_dictionary_values Excepcion: {1}".format(str(e)))
    

# Datos para leer el archivo (inicio y fin de lectura)
def get_beggining_data(rank, size):
    global total_lines
    total_lines = sum(1 for line in log_file) // 1000 # Cantidad de lineas en el archivo log_file
    wload = total_lines // size
    begin = rank * wload
    rest = total_lines % size
    return [wload, begin, rest]

def main():
    global countries_dictionary
    global cities_dictionary
    global ip_dictionary
    global user_dictionary
    global hour_dictionary
    global line
    global argumento
    global time_mark

    attack_string = "error=authentication failed"
    
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # Fila de inicio de lectura y cantidad de lineas a leer)
    wload, begin, rest = get_beggining_data(rank, size)
    end = begin + wload
    print("Nodo{0}: wload: {1} begin{2} end: {3} rest{4}".format(rank, wload, begin, end, rest))
    # Devolver el buffer al principio del documento
    # (despues de contar la cantidad de lineas)
    log_file.seek(0, 0)

    try:
        walk = 0 # Indicador de posicion de linea
        while walk < begin: # Recorrer las lineas que no se leeran
            log_file.readline()
            walk += 1

        line = log_file.readline() # Leer linea
        if attack_string in line:
            attacks_by_ip() # Analizar string y obtener datos
            attacks_by_user() # Analizar user y obtener datos
            attacks_by_hour() # Analizar hora y obtener datos
            attacks_by_day() # Analizar dia y obtener datos
            attacks_by_protocols() # Analizar protocolo y obtener datos
        else:
            success_protocols_connections() # Conexion exitosa
        walk += 1

        time_mark = get_date_from_string(line)

        while walk < end: # Leer lineas asignadas al nodo
            line = log_file.readline() # Leer linea
            print("Nodo {0}: Leyendo linea {1}".format(rank, walk))
            if attack_string in line:
                attacks_by_ip() # Analizar string y obtener datos
                attacks_by_user() # Analizar user y obtener datos
                attacks_by_hour() # Analizar hora y obtener datos
                attacks_by_day() # Analizar dia y obtener datos
                attacks_by_protocols() # Analizar protocolo y obtener datos
            else:
                success_protocols_connections() # Conexion exitosa
            walk += 1
        
        # Si eres el ultimo nodo y hay resto, lee las lineas faltantes
        if rank == size-1 and rest > 0: 
            while rest > 1:
                print("Nodo {0}: Leyendo linea faltante: {1}".format(rank, rest))
                line = log_file.readline() # Leer linea
                if attack_string in line:
                    attacks_by_ip() # Analizar string y obtener datos
                    attacks_by_user() # Analizar user y obtener datos
                    attacks_by_hour() # Analizar hora y obtener datos
                    attacks_by_day() # Analizar dia y obtener datos
                    attacks_by_protocols() # Analizar protocolo y obtener datos
                else:
                    success_protocols_connections() # Conexion exitosa
                rest -= 1
            
            print("Nodo {0}: Terminada de leer lineas faltante. Resto: {1}".format(rank, rest))

        log_file.close() # Cerrar el archivo

        #########################################################################################
        ############################# Recolectando Información ##################################
        #########################################################################################

        if rank == 0:
            for i in range(1, size):
                print("Nodo {0}: Recolectando datos - Ciclo: {1}".format(rank, i))
                dictionaries_list = comm.recv(source=i, tag=1)
                add_dictionary_values(countries_dictionary, dictionaries_list[0])
                add_dictionary_values(cities_dictionary, dictionaries_list[1])
                add_dictionary_values(ip_dictionary, dictionaries_list[2])
                add_dictionary_values(user_dictionary, dictionaries_list[3])
                add_dictionary_values(hour_dictionary, dictionaries_list[4])
                add_dictionary_values(days_dictionary, dictionaries_list[5])
                add_dictionary_values(protocols_dictionary, dictionaries_list[6])
                add_dictionary_values(success_protocols_dictionary, dictionaries_list[7])
                #add_dictionary_values(attacks_average, dictionaries_list[8])
        else:
            dictionaries_list = [countries_dictionary, cities_dictionary, ip_dictionary, user_dictionary,
            hour_dictionary, days_dictionary, protocols_dictionary, success_protocols_dictionary]
            comm.send(dictionaries_list, dest=0, tag=1)
            #print("Nodo {0}: - Diccionario enviado.".format(rank))
            #print("Nodo {0}: - Attacks Average length: {1}".format(rank, len(attacks_average)))
            

        #########################################################################################
        ############################## Imprimiendo Información ##################################
        #########################################################################################

        if rank == 0:

            # Attack Average
            #print("\n### ATTACK AVERAGE ###")
            #for key in attacks_average:
                # promedio = "Ataque por cada {0} minutos: {1}".format(
                # parametro_minutos, round(top_20_countries[i][1] / cada_x_minutos, 2) )
                #print("Pais {0}: {1}".format(key, attacks_average[key]))

            # Top de los 20 paises segun IP en los que se produjeron más ataques 
            print("\n### TOP 20 paises atacantes ###")
            top_20_countries = sorted(countries_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_20_countries)):
                # promedio = "Ataque por cada {0} minutos: {1}".format(
                # parametro_minutos, round(top_20_countries[i][1] / cada_x_minutos, 2) )
                print("Pais {0}: {1}".format(i, top_20_countries[i]))
            

            # Top de las 20 ciudades segun IP en los que se produjeron más ataques
            print("\n### TOP 20 ciudades atacantes ###")
            top_20_cities =  sorted(cities_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_20_cities)):
                # promedio = "Ataque por cada {0} minutos: {1}".format(
                # parametro_minutos, round(top_20_cities[i][1] / cada_x_minutos, 2) )
                print("Ciudad {0}: {1} ".format(i, top_20_cities[i]))

            
            # Top de los 20 IP en los que se produjeron más ataques
            print("\n### TOP 20 IPs atacantes ###")
            top_20_IPs =  sorted(ip_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_20_IPs)):
                # promedio = "Ataque por cada {0} minutos: {1}".format(
                # parametro_minutos, round(top_20_IPs[i][1] / cada_x_minutos, 2) )
                print("IP {0}: {1}".format(i, top_20_IPs[i]))

            
            # Top de los 20 Usuarios que realizaron mas ataques
            print("\n### TOP 20 Emails atacantes ###")
            top_20_Emails =  sorted(user_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_20_Emails)):
                # promedio = "Ataque por cada {0} minutos: {1}".format(
                # parametro_minutos, round(top_20_Emails[i][1] / cada_x_minutos, 2) )
                print("Email {0}: {1}".format(i, top_20_Emails[i]))


            # Top de las 20 Horas que se realizaron mas ataques
            print("\n### TOP 20 Horas de mas ataques ###")
            top_20_Hours =  sorted(hour_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_20_Hours)):
                print("Hora {0}: {1}".format(i, top_20_Hours[i]))

            

            ########################### Estadísticas Personalizadas #############################
            
            total_attacks = protocols_dictionary["Imap"] + protocols_dictionary["Pop"] + protocols_dictionary["soap"]
            print("\n Diccionario de ataques por protocolo: {0}".format(protocols_dictionary))
            total_successfull_connections = success_protocols_dictionary["Imap"] + success_protocols_dictionary["Pop"] + success_protocols_dictionary["soap"]
            print("\n Diccionario de conexiones por protocolo: {0}".format(success_protocols_dictionary))


            # Top protocolos con mas ataques
            print("\n### TOP protocolos con mas ataques ###")
            top_3_protocols =  sorted(protocols_dictionary.items(), key=lambda p:p[1], reverse=True)[:3]
            for i in range(len(top_3_protocols)):
                print("Protocolo {0}: {1}".format(i, top_3_protocols[i]))


            # Porcentaje de ataques por protocolo
                print("Porcentaje de ataques por protocolo {0}: {1}%".format(
                    top_3_protocols[i][0], round( top_3_protocols[i][1] / total_attacks, 4 ) * 100
                ))


            # Top protocolos con mas conexiones exitosas
            print("\n### TOP protocolos con mas conexiones exitosas ###")
            top_3_success_protocols =  sorted(success_protocols_dictionary.items(), key=lambda p:p[1], reverse=True)[:3]
            for i in range(len(top_3_success_protocols)):
                print("Protocolo {0}: {1}".format(i, top_3_success_protocols[i]))


            # Porcentaje de conexiones por protocolos
                print("Porcentaje de conexiones por protocolo {0}: {1}%".format(
                    top_3_success_protocols[i][0], round( top_3_success_protocols[i][1] / total_successfull_connections, 4 ) * 100
                ))


            # Top dias con mas ataques
            print("\n### TOP dias con mas ataques ###")
            top_days =  sorted(days_dictionary.items(), key=lambda p:p[1], reverse=True)[:20]
            for i in range(len(top_days)):
                print("Dia {0}: {1}".format(i, top_days[i]))


            # Porcentaje de ataques por dia
                print("Porcentaje de ataques por dia {0}: {1}%".format(
                    top_days[i][0], round( top_days[i][1] / total_attacks, 4 ) * 100
                ))


            # Top de los 5 paises segun IP en los que se produjeron menos ataques 
            print("\n### TOP 5 paises menos atacantes ###")
            top_5_countries = sorted(countries_dictionary.items(), key=lambda p:p[1])[:5]
            for i in range(len(top_5_countries)):
                print("Pais {0}: {1}".format(i, top_5_countries[i]))


            # Top de las 5 ciudades segun IP en los que se produjeron menos ataques
            print("\n### TOP 5 ciudades menos atacantes ###")
            top_5_cities =  sorted(cities_dictionary.items(), key=lambda p:p[1])[:5]
            for i in range(len(top_5_cities)):
                print("Ciudad {0}: {1}".format(i, top_5_cities[i]))


            # Top de los 5 IP en los que se produjeron menos ataques
            print("\n### TOP 5 IPs menos atacantes ###")
            top_5_IPs =  sorted(ip_dictionary.items(), key=lambda p:p[1])[:5]
            for i in range(len(top_5_IPs)):
                print("IP {0}: {1}".format(i, top_5_IPs[i]))


            # Top de los 5 Usuarios que realizaron menos ataques
            print("\n### TOP 5 Emails con menos atacantes ###")
            top_5_Emails =  sorted(user_dictionary.items(), key=lambda p:p[1])[:5]
            for i in range(len(top_5_Emails)):
                print("Email {0}: {1}".format(i, top_5_Emails[i]))


            # Top de las 5 Horas que se realizaron menos ataques
            print("\n### TOP 5 Horas con menos ataques ###")
            top_5_Hours =  sorted(hour_dictionary.items(), key=lambda p:p[1])[:5]
            for i in range(len(top_5_Hours)):
                print("Hora {0}: {1}".format(i, top_5_Hours[i]))

    except Exception as e:
    	print(str(e))
		#print("Nodo {0}: Excepcion: {1}".format(rank, str(e)))

main()
